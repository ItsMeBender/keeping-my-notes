# Contributing

Hi, it's nice if you want to contribute in this project.

Currently the impact of the project is small. I have one goal;

> Share a basic Metalsmith site, to store and keep personal notes organized.
> Just for fun and to learn more about Metalsmith, JavaScript, HTML5, CSS3, etc.

If you have other ideas, improvements, please add them.

__ItsMeBender__  
_Bending the future._
