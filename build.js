#!/usr/bin/env node

// Metalsmith build file
// Build site with `node ./build.js` or `npm start`
// Build production site with `npm run production`

'use strict';

/* eslint no-process-env: ["off"] */
/* eslint dot-location: ["off"] */

// Development Defaults
const consoleLog = false; // set true for metalsmith file and meta content logging
const devBuild = (process.env.NODE_ENV || '').trim().toLowerCase() !== 'production'; // Type of build

// Modules
const assets = require('metalsmith-assets'); // Copy SITE assets; images, fonts, etc.
const autotoc = require('metalsmith-autotoc'); // Creates Table Of Content based on H2 ... H6
const browsersync = devBuild ? require('metalsmith-browser-sync') : null; // Browser synchronisation
const collections = require('metalsmith-collections'); // Create Collections on subjects
const discoverPartials = require('metalsmith-discover-partials'); // Register Handlebars (.hbs) partials
const frontMatter = require('metalsmith-default-values'); // Set default Metalsmith values (Page Frontmatter)
const handlebars = require('handlebars'); // To created Handlebars block function `ifOnPageWithID`
const highlight = require('highlight.js'); // Format code examples
const htmlmin = devBuild ? null : require('metalsmith-html-minifier'); // minify HTML on PRODUCTION
const inplace = require('metalsmith-in-place'); // Prepare for Handlebars in Metalsmith
const layouts = require('metalsmith-layouts'); // Handlebars layouts
const marked = require('marked'); // https://marked.js.org/#/USING_ADVANCED.md
const metalsmith = require('metalsmith'); // Metalsmith CORE
// const permalinks = require('metalsmith-permalinks'); // Create permalinks; ./dirA/dirB/index.html
const path = require('path'); // Working with URL path()
const pkg = require('./package.json'); // Access to version number and description in 'package.json'
// const publish = require('metalsmith-publish'); // If marked`publish` then publish on site
const sass = require('metalsmith-sass'); // Use SASS to CSS
// const sitemap = require('metalsmith-mapsite'); // Create a site map
const tags = require('metalsmith-tags'); // Document, content tagging
const wordcount = require("metalsmith-word-count"); // Count words and reading time for an article
const dir = { // main directories
    base: path.join(__dirname, '/'),
    lib: path.join(__dirname, '/lib/'),
    source: './src/',
    dest: './build/'
};

// Custom 'Homemade' plugins
const debug = consoleLog ? require(dir.lib + 'metalsmith-debug') : null;
const prepareCollections = require(dir.lib + 'metalsmith-prepare-collections');
const setdate = require(dir.lib + 'metalsmith-setdate');
const tagCloud = require(dir.lib + 'metalsmith-tagcloud');
const words = require(dir.lib + 'metalsmith-words');
const siteMeta = {
    author: 'Put your name here',
    contact: 'email:your@website.com',
    desc: 'A demonstration static site built using Metalsmith',
    devBuild: devBuild,
    domain: devBuild ? 'http://127.0.0.1' : 'http://your.website.com',
    name: 'Name of the site',
    // rootPath: devBuild ? null : '', // set absolute path (null for relative)
    version: pkg.version
};

/**
 * Order collection list on ALPHABET Page title.
 * @param {String} a compate file A with B
 * @param {String} b compate file B with A
 * @return {number} sort outcome -1, 0, 1 = less, equal, more
 */
const alfabet = (a, b) => {
    a = a.title;
    b = b.title;
    if (!a && !b) return 0;
    if (!a) return -1;
    if (!b) return 1;
    if (b > a) return -1;
    if (a > b) return 1;
    return 0;
};

// --------------------------------------------------------------------------------------------
// Metalsmith CONFIGURATIONS
// --------------------------------------------------------------------------------------------

const configCollections = { // Determine page collection/taxonomy
    blender3D: {
        pattern: 'blender-3d/**/*',
        refer: true,
        reverse: false,
        sortBy: alfabet
    },
    notes: {
        pattern: 'notes/**/*',
        refer: true,
        reverse: false,
        sortBy: alfabet
    },
    mainNavigation: { // This is the upper navigation level, used for TABS. TODO: rename
        pattern: '**/index.*', // Find ALL 'index' pages.
        refer: true,
        reverse: true,
        sortBy: 'priority'
    },
    project: {
        pattern: 'projects/**/*',
        refer: true,
        reverse: false,
        sortBy: alfabet
    },
    software: {
        pattern: 'software/**/*',
        refer: true,
        reverse: false,
        sortBy: alfabet
    }
};

const configDefaultFrontmatter = [
    { // Based on collections
        pattern: ['blender-3d/*', 'notes/*', 'projects/*', 'software/*'],
        defaults: {
            rootPath: '../'
        }
    },
    {
        pattern: 'topics/*',
        defaults: {
            isTopic: true,
            rootPath: '../',
            title: 'Index'
        }
    }
];

const configInPlace = {
    engineOptions: {
        highlight: (code) => highlight.highlightAuto(code).value,
        renderer: new marked.Renderer() // Instance of the 'Marked' (Markdown) renderer, to overwrite default ELEMENT formatting.
    }
};

// Configuration for 'Marked' a Markdown interpreter (more functions).
// https://marked.js.org/#/USING_ADVANCED.md#options
const configMarked = {
    default: 'page.hbs',
    directory: path.join(dir.source, 'template/'),
    engine: 'marked',
    partials: path.join(dir.source, 'partials/'),
    engineOptions: {
        headerIds: true,
        headerPrefix: "hdr-"
    }
};

const configSass = {
    outputDir: 'css/', // This changes the output dir to "build/css/" instead of "build/scss/"."
    outputStyle: 'expanded',
    sourceMap: true,
    sourceMapContents: true // This will embed all the Sass contents in your source maps.
};

const configTags = {
    handle: 'tags',
    path:'topics/:tag.html',
    layout: '../../src/template/tagged.hbs',
    sortBy: 'date',
    reverse: true,
    skipMetadata: false,
    metadataKey: "tagCategory",
    slug: {mode: 'rfc3986'}
};

const configTagCloud = {
    "handle": "tags",
    "path": "topics/:tag.html",
    "reverse": false
};

// --------------------------------------------------------------------------------------------
// Metalsmith plugin Expansion
// --------------------------------------------------------------------------------------------

// Handlebars BLOCK HELPER
// https://stackoverflow.com/questions/8853396/logical-operator-in-a-handlebars-js-if-conditional
handlebars.registerHelper('ifOnPageWithID', function(v1, v2, options) {
    if (v1 === v2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

handlebars.registerHelper('toLowerCase', function(str) {
    return str.toLowerCase();
});

const getHrefPrams = (hrefStr) => {
    let href = '';
    let params = {};

    if (hrefStr) {
        const match = /(^.+?)( \?(.*))?$/.exec(hrefStr);
        const extraParams = match[3] ? match[3].split(',') : [];
        href = match[1];
        params = extraParams.reduce((map, obj) => {
            const split = obj.split('=');
            map[split[0]] = split[1];
            return map;
        }, {});
    }

    return {href, params};
};

// Rewrite IMG-tag generator.
// ![alt text](image.png ?w=180,h=360,align=right,float=right,inline=true,external=true,make_link=true "title")
configInPlace.engineOptions.renderer.image = function(hrefStr, title, alt) {
    const closing = this.options.xhtml ? '/>' : '>';
    const {href, params} = getHrefPrams(hrefStr);
    const attrs = [`alt="${alt}"`, `src="${href}"`];
    const divClasses = []; // When IMG-tag rendered inside a DIV-block.
    const imgClasses = []; // Add default CSS classses

    if (title) { // Preparing IMG-element ATTRIBUTES
        attrs.push(`title="${title}"`);
    }
    if (params.w) { // Physical dimensions
        attrs.push(`width="${params.w}"`);
    }
    if (params.h) {
        attrs.push(`height="${params.h}"`);
    }
    if (params.class) { // Set CSS class
        imgClasses.push(params.class);
    }
    if (params.inline) { // Classes using DIB block
        divClasses.push('inline');
    }
    if (params.align) { // Defaults to left aligned
        divClasses.push(`text-${params.align}`);
    }
    if (params.float) {
        if (params.make_link) {
            divClasses.push(`float-${params.float}`);
        } else {
            imgClasses.push(`float-${params.float}`);
        }
    }
    if (imgClasses.length) { // Build class-attribute string.
        const classesJoin = imgClasses.join(' ');
        attrs.push(`class="${classesJoin}"`);
    }
    const attributes = attrs.join(' '); // build element-attributes
    const img = `<img ${attributes}${closing}`; // Return new IMG-element as IMG or imbedded in DIV

    return params.make_link
        ? `<div class="${divClasses.join(' ')}"><a target="_blank" href="${href}">${img}</a></div>`
        : img;
};

// RSS Feed, Site Map
siteMeta.site_url = siteMeta.domain + (siteMeta.rootPath || '');
siteMeta.url = siteMeta.domain + (siteMeta.rootPath || '');
siteMeta.root = siteMeta.domain + (siteMeta.rootPath || '');
siteMeta.title = siteMeta.name;
siteMeta.description = siteMeta.desc;

console.log(devBuild ? 'Development' : 'PRODUCTION', 'build, version', pkg.version);
console.log(siteMeta.url);

var ms = metalsmith(dir.base)
    .clean(!devBuild) // clean folder before a production build
    .source(dir.source + 'html/') // source folder (src/html/)
    .destination(dir.dest) // build folder (build/)
    .metadata(siteMeta) // add meta data to every page
    .use(words())
    .use(sass(configSass))
    .use(tags(configTags))
    .use(tagCloud(configTagCloud))
    // .use(publish()) // draft, private, future-dated
    .use(setdate()) // set date on every page if not set in front-matter
    .use(collections(configCollections))
    // .use(permalinks({pattern: ':mainCollection/:title'}))
    .use(discoverPartials({directory: './src/partials/', pattern: /\.hbs$/}))
    .use(frontMatter(configDefaultFrontmatter))
    .use(prepareCollections()) // determine root paths and navigation
    .use(inplace(configInPlace)) // in-page templating
    .use(autotoc()) // Table Of Contents
    .use(wordcount()) // word count
    .use(layouts(configMarked)); // layout templating

if (htmlmin) ms.use(htmlmin()); // minify production HTML

if (debug) ms.use(debug()); // output page debugging information

if (browsersync) ms.use(browsersync({ // start test server
    server: dir.dest,
    files: [dir.source + '**/*']
}));

ms
    // .use(sitemap({hostname: siteMeta.domain + (siteMeta.rootPath || ''), omitIndex: true}))
    .use(assets({ // copy assets: CSS, images etc.
        source: dir.source + 'assets/',
        destination: './'
    }))
    .build(function(err) { // build
        if (err) throw err;
    });
