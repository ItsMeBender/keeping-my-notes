# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.1] - 2019-04-05
### Changed
- General site header meta information.

## [1.1.0] - 2019-04-04
### Added
- The basic project structure. Dirty as it is.

