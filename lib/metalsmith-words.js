#!/usr/bin/env node
/**
 * Scan all pages for words in content.
 * And count these words, if they are NOT in the `stats.json` file.
 * Which acts as an exclude option.
 * - I use this to see if I can do some SEO.
 * - And as a prelude to implement a LOCAL SITE SEARCH for my notes.
 */

let fs = require('fs'); // Read the stats.json file
let path = require('path');

/* global Map */

/**
 * Expose `plugin`.
 */

module.exports = plugin;

/**
 * Metalsmith plugin to create a sorted weighted list of articles categories/tags based
 * @param {String | Object} opts plugin options
 * @property {String} categories (optional) Default is 'tags'.  Tell plugin which property to pull to get data
 * @property {boolean} reverse Should the category sort be reversed.  Default is false, thus sort is descending alphanumeric
 * @return {Function} plugin function that is run by metalsmith
 */
function plugin(opts) {
    let excl; // An array of excluded words, see file 'stats.json'.
    let words = new Map();

    opts = opts || {};

    fs.readFile(path.join(__dirname, '../stats.json'), (err, data) => {
        if (err) {
            throw err;
        }

        processJSONFile(data); // Or put the next step in a function and invoke it
    });

    function processJSONFile (data) {
        excl = JSON.parse(data).exclude;
    }

    function addWord (w, l) {
        if (words.has(w)) {
            // excisting word, count
            words.get(w).c += 1;
        } else {
            // New word, add to the list
            words.set(w, {
                c: 1, // Counter
                f: [] // Files
            });
        }
    }

    function checkWord (i) {
        i = i.trim().toLowerCase();
        if (i !== '') {
            if (i.length > 2) {
                if (excl.indexOf(i) === -1) {
                    addWord(i);
                }
            }
        }
    }

    function logMapElements(value, key, map) {
        console.log(`${key} ${value.c}`);
    }

    function showWords () {
        let sorted = new Map([...words.entries()].sort((a,b) => {
                // console.log("a:",a[1].c);
                return a[1].c > b[1].c;
            })
        );
        let m = new Array(...sorted.entries());
        let i;

        m.reverse();

        console.log(`\nOverview of 20 most used words (./lib/metalsmith-words.js):\n`);
        for (i = 0;i < 20;i += 1) {
            console.log(`${i + 1}: ${m[i][0]} - ${m[i][1].c}`);
        }

        // sorted.forEach(logMapElements);
        // words.forEach(logMapElements);
    }

    /**
     * returned function called by MetalSmith to implement plugin
     * @param  {[type]}   files      list of html pages created from markdown
     * @param  {[type]}   metalsmith primary object for managing workflow
     * @param  {Function} done       function called to exit plugin and return control to parent
     * @return {[type]}              function to be called to complete plugin work
     */
    return function(files, metalsmith, done) {
        // let metadata = metalsmith.metadata();
        let f;

        for (f in files) {
            let file = files[f];

            if (path.extname(f) === '.hbs') {
                let w = [];
                let c = file.contents.toString('utf-8');
                c = c.replace(/(\r\n|\n|\r|\t)/gm,"");
                c = c.replace(/[()0-9=!?:;,<>%'"{}-]/gm, " ");
                c = c.replace(/\s\s|_|\.|\+|\*|#|\[|\]|\/|\\|'|`/g, " ");

                w = c.split(" ");

                w.forEach(checkWord);
            }
        }

        showWords();

        done();
    };
}
