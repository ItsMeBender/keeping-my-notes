// metalsmith-debug
// displays site metadata and page information in the console
module.exports = function() {
    'use strict';

    return function(files, metalsmith, done) {
        console.log('\nMETADATA Files:');
        // console.log(metalsmith.metadata()); // Dumps all, but too much
        console.log(Object.keys(files));
        // console.log('\nMETADATA properties for "notes/html-css.html":');
        // console.log(Object.keys(files['notes/html-css.html']));
        Object.keys(files['notes/html-css.html']).forEach(function(prop) {
            var t = files['notes/html-css.html'][prop].toString();
            t = t.substr(0, 20);
            console.log(`'${prop}': ${t} (typeof ${typeof files['notes/html-css.html'][prop]})`);
        });

        console.log('\nMETADATA properties values:');
        console.log('Title', files['notes/html-css.html'].title);
        console.log('root, path', files['notes/html-css.html'].root, files['notes/html-css.html'].path);

        console.log('\nMETADATA STATS properties for "notes/html-css.html":');
        console.log(Object.keys(files['notes/html-css.html'].stats, "\n\n"));

        for (var f in files) {
            // console.log(`\nPAGE: ${files[f]}`);
            // console.log(files[f]);
            // console.log("toc", f, files[f].toc);
        }

        // console.log(files['notes/html-css.html']);
        // console.log("toc", files['index.html'].toc);
        done();
        // console.log("toc", files["index.html"].navmain);
        // console.log("toc", files["index.html"].toc);
    };
};
