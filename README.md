![logo](/src/assets/apple-touch-icon.png) 

# Project Notes

This project is about a website that is being built and maintained with the __[Metalsmith](https://metalsmith.io/) static site generator__.

A copy of this project is used to store my personal notes on projects and other things of interest.

Why, because of the simplicity of the [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) markup, that I use a lot in my daily work. But keeping track of all those scattered `.md` files was difficult.

I cannot say that this is the best solution. But thanks to the flexibility of __Metalsmith__, it gave me improved access to knowledge stored in those scattered project notes.



## How to begin

- `nodejs` and `npm` being installed.
- Download a copy of this project
- Open a terminal session
- run `npm install`, downloading all software needed for this project as declared in `package.json`.
- run `npm start`.  
A site is being generated, using the _'./build'_ folder.  
When finished, a web browser will show this site as localhost.

Now you can start adding or changing notes in the existing collections. See `./src/html/`.  
To __add__ new collections or change their existing (folder) names, 
you need to configure the `./build.js` file. This is the control center of _Metalsmith_.
For this you need to follow _Metalsmith_ and other plugin documentation.



## What does it provide

- When run `npm start`, it compiles a fresh version of all stored notes and starts a local web server to host the website.
- It runs localy on your PC (private). Or it runs on a internet server (public).
- Content is grouped in collections, identified by folders in `./src/html/*`.
And configured in `./build.js`. 
- Each group has an `index.md.hbs` file which acts as _index_ for this collection. 
This page provides an overview of all notes in this collection folder, depending on the choosen __Handlebars__ template.
- Per collection jou can add many notes `myproject-01.md.hbs`, `myproject-02.md.hbs` etc.
- Each note page has a table of contents, configurable using `autotoc: true` in the __frontmatter__ of the notes page.
- Each note can be identifies with __tags__, see `tags: xxx, yyy, zzz` in the __frontmatter__ of the notes page.
- A label cloud is created, containing those __tags__ for quick navigation to other notes.



## Based on

__Metalsmith__ is flexible in the options that you choose. In the past I have made a number of decisions that may not be so obvious these days. To be honest, this project was all about learning how to work with Metalsmith. So I experimented a lot.
And used different development styles, from other projects and renewed insights.

This is why I use;

- [Handlebar](https://handlebarsjs.com/), minimal templating on steroids. Because it looked simple.
- `./css/googlecode.css`. to format JavaScript code, based on Google standards, as JavaScript examples in notes.
- Added some additional libaries, see `./lib`.
- SASS as preprocessor for styling CSS.



## Known issues

Some of the issues I am aware of;

- When in __watch mode__ (`npm start`), while working on content, 
collections indexes in the `./build/` folder may duplicate. 
Causing duplication of menus and other collection indexes.
It's a bug outside this project scope.  
__Work around__;  
a. Stop and Start the web server (basically redo: `npm start`),  
b. Stop and build for production (does some minification), use `npm run production`.
- There are some configuration differences between Windows and Linux (I use Ubuntu).
See __package.json__ `run production` vs `run winprod`.
- Not tested with the diversity of browser versions.
- A diversity of JavaScript standards and versions.
- No solid JavaScript documentation.
- Some misunderstanding / configuration of `rootPath` (due to no Metalsmith experience). 
- Including _waypoints_ (`./js/vendor/noframework.waypoints.min.js`), to improve usabillity. But not yet active.
- I see also [Electron](https://electronjs.org/) is installed, because I had an experiment to implement this site as an application.
- It's not a clean project
