(function(scope) {

	/**
	 * Set the width of the side navigation to 0. Hiding the menu.
	 */
	function closeSideNav() {
		document.querySelector('#overlay').hidden = true;
		document.querySelector('body').classList.remove('has-side-navigation');
	} 

	/**
	 * Does the browser viewport in relation with the content, activates the vertical scrollbar.
	 * @param {element} element as reference for scrollbar
	 * @returns true then a scrollbar is active (visible)
	 */
	function isScrollbarVisible(element) {
		// https://stackoverflow.com/questions/4814398/how-can-i-check-if-a-scrollbar-is-visible
		var pw = document.querySelector(element);
		console.log('scrollHeight', pw.scrollHeight, ', clientheight', pw.clientHeight);
		return pw.scrollHeight > pw.clientHeight;
	}

	/**
	 *  Set the width of the side navigation to 250px. Showing the menu.
	 */
	function openSideNav() {
		document.querySelector('#overlay').hidden = false;
		document.querySelector('body').classList.add('has-side-navigation');
	}

	function resizeImg(e) {
		var node = e.target;
		if (node.getAttribute('style')) {
			node.removeAttribute('style');
		} else {
			node.setAttribute('style', 'max-width: 100%;');
		}
		
	}

	// Event handling
	document.querySelector('#menuBTN').addEventListener('click', (e) => {openSideNav();});
	document.querySelector('#closeBTN').addEventListener('click', (e) => {closeSideNav();});
	document.querySelector('#overlay').addEventListener('click', (e) => {closeSideNav();});

	// Images Event Handling
	var imgNodeLst = document.querySelectorAll('.js__zoom');
	if (imgNodeLst) {
		imgNodeLst.forEach(node => {
			node.addEventListener('click', resizeImg);
		});
	}

	// Page copyright, content meta.
	document.querySelector('footer .disclaimer').innerHTML = '&copy; Copyright ' + new Date().getFullYear();
	
})(window);
